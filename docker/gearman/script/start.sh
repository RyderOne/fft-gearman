#!/usr/bin/env bash
set -o errexit

GEARMAN_LOG_DIR="/var/log/gearman"
LOG_FILE="$GEARMAN_LOG_DIR/gearmand.log"

if [ ! -d "$GEARMAN_LOG_DIR" ];
    then
    mkdir $GEARMAN_LOG_DIR
fi

if [ ! -e "$LOG_FILE" ];
    then
    touch $LOG_FILE
fi


gearmand --log-file $LOG_FILE --verbose NOTICE