FROM ubuntu:16.04

# Install Requirements
RUN apt-get update -qq && apt-get install -qqy \
    sudo \
    wget \
    curl \
    git \
    apt-utils \
    acl \
    openssl \
    graphviz \
    nano \
    htop \
    nginx \
    libgearman-dev \
    gearman-job-server \
    supervisor \
    unzip \
    re2c \
    && echo "Europe/Paris" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata \
    && echo 'alias ll="ls -lah --color=auto"' >> /etc/bash.bashrc

# Repository PHP
RUN mkdir /run/php \
    && apt-get update -qq && apt-get install -qqy \
    software-properties-common \
    python-software-properties \
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

# PHP
ENV PHP_VERSION=7.0
RUN apt-get update -qq && apt-get install -qqy \
    php$PHP_VERSION \
    php$PHP_VERSION-cli \
    php$PHP_VERSION-intl \
    php$PHP_VERSION-mysql \
    php$PHP_VERSION-curl \
    php$PHP_VERSION-xdebug \
    php$PHP_VERSION-gd \
    php$PHP_VERSION-xml  \
    php$PHP_VERSION-fpm \
    php$PHP_VERSION-dev \
    php$PHP_VERSION-redis

# Install Gearman from source (php-gearman extension causes segfault)
WORKDIR /tmp
RUN wget https://github.com/wcgallego/pecl-gearman/archive/master.zip && unzip master.zip
WORKDIR /tmp/pecl-gearman-master
RUN phpize \
    && ./configure \
    && make install \
    && echo "extension=gearman.so" > /etc/php/$PHP_VERSION/mods-available/gearman.ini \
    && phpenmod -v ALL -s ALL gearman \
    && rm -rf /tmp/master.zip /tmp/pecl-gearman-master

RUN version=$(php -r "echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;") \
    && curl -A "Docker" -o /tmp/blackfire-probe.tar.gz -D - -L -s https://blackfire.io/api/v1/releases/probe/php/linux/amd64/$version \
    && tar zxpf /tmp/blackfire-probe.tar.gz -C /tmp \
    && mv /tmp/blackfire-*.so $(php -r "echo ini_get('extension_dir');")/blackfire.so \
    && printf "extension=blackfire.so\nblackfire.agent_socket=tcp://blackfire:8707\n" > /etc/php/$PHP_VERSION/cli/conf.d/blackfire.ini \
    && printf "extension=blackfire.so\nblackfire.agent_socket=tcp://blackfire:8707\n" > /etc/php/$PHP_VERSION/fpm/conf.d/blackfire.ini

# Sources
WORKDIR /var/www

EXPOSE 443

COPY script/start.sh /root/start.sh
COPY script/exportWorker.conf /etc/supervisor/conf.d/exportWorker.conf

CMD ["/bin/bash", "/root/start.sh"]
