# ================================================================================
# PROJECT VARS - YOU CAN UPDATE
# ================================================================================
step=/////////////////////
project=fft-gearman
projectCompose=fft-gearman
console_path=app/console
db_name=fft-gearman
db_user=root
db_pwd=fft-gearman
makefiles_path=.zol/zol-common/makefile
cache_log_prefix=var
# ================================================================================
# COMMON VARS - YOU MUST NOT UPDATE
# ================================================================================

ifeq "$(wildcard $(makefiles_path) )" "$(makefiles_path)"
    include $(makefiles_path)/assets.mk
	include $(makefiles_path)/common.mk
    include $(makefiles_path)/composer.mk
    include $(makefiles_path)/docker.mk
    include $(makefiles_path)/hooks.mk
    include $(makefiles_path)/mysql.mk
    include $(makefiles_path)/proxy.mk
    include $(makefiles_path)/symfony.mk
endif

# ================================================================================
# CUSTOM
# ================================================================================

$(call check_defined, USER_CMD)

install-app: remove clean-app build start composer-install js-install gulp cache-clear 
install-db: database fixtures

install: install-app install-db

zol-common:
	@rm -rf .zol
	@mkdir -p .zol
	@cd .zol && git clone --branch v3.1.10 git@gitlab.com:zolteam/zol-common.git

copy-certs:
	@sudo cp docker/web/conf/dev/certs/server.* ~/.zol/nginx/certs

mysql:
	@echo "$(step) MySQL $(project) $(step)"
	$(compose) run --rm db /bin/bash -c "mysql -hdb -u${db_user} ${db_name} -p${db_pwd}"

restart-export-worker:
	@echo "$(step) Restart export worker $(step)"
	@docker exec fftgearman_web_1 sh -c 'supervisorctl restart exportWorker:'
