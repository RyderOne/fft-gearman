<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Start a gearman worker.
 */
class StartWorkerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gearman:worker:start')
            ->setDescription('Start workers for gearman')
            ->addArgument('worker', InputArgument::REQUIRED, 'The worker class name, camel case')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $workerName = $input->getArgument('worker');
        $qualifiedClassName = 'AppBundle\\Gearman\\'.ucfirst($workerName);

        if (!class_exists($qualifiedClassName)) {
            throw new \Exception('Unknown worker '.$workerName.'. Make sure this class exist and is reachable at '.$qualifiedClassName);
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $kernelLogsDirectory = $this->getContainer()->getParameter('kernel.logs_dir');
        $kernelRootDirectory = $this->getContainer()->getParameter('kernel.root_dir');
        $gearmanLogDirectory = $this->getContainer()->getParameter('gearman_log_directory');
        $exportDirectory = $this->getContainer()->getParameter('export_directory');

        switch ($workerName) {
            case 'ExportWorker':
                $worker = new $qualifiedClassName($em, $kernelLogsDirectory.'/'.$gearmanLogDirectory, $kernelRootDirectory.'/../web/'.$exportDirectory);
                break;
        }

        $worker->work();
    }
}
