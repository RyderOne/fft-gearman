<?php

namespace AppBundle\Helper;

use Blackfire\ClientConfiguration;
use Blackfire\Client;

class BlackfireHelper
{
    public static $blackfire;
    public static $probe;

    public static function start()
    {
        $blackfireConfig = new ClientConfiguration(getenv('BLACKFIRE_CLIENT_ID'), getenv('BLACKFIRE_CLIENT_TOKEN'));
        self::$blackfire = new Client($blackfireConfig);
        self::$probe = self::$blackfire->createProbe();
    }

    public static function end()
    {
        $profile = self::$blackfire->endProbe(self::$probe);
        return $profile->getUrl();
    }
}