<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    const AMOUNT_USERS = 1000;

    public function load(ObjectManager $em)
    {
        $firstNames = ['Thierry', 'Benjamin', 'Christophe', 'Mathieu', 'Romain', 'Tristan', 'Virgile', 'Florent', 'Antoine', 'Yannick', 'Kévin', 'Félicienne', 'Eric', 'Xavier'];

        $lastNames = ['Meissonnier', 'Hautenne', 'Boivin', 'Molimard', 'Tuaillon', 'Richard', 'Martin', 'Geindre', 'Petit', 'Pettinotti', 'Pereira-Reis', 'Patel', 'De Contencin', 'Reneleau', 'Lavy', 'Boisson'];

        for ($i=0; $i < self::AMOUNT_USERS; $i++) {
            $firstName = $firstNames[rand(0, count($firstNames) - 1)];
            $lastName = $lastNames[rand(0, count($lastNames) - 1)];

            $user = (new User())
                ->setUsername(strtolower($firstName.'_'.$lastName))
                ->setEmail(str_replace(' ', '-', strtolower($firstName.'.'.$lastName).'@zol.fr'))
                ->setFirstName($firstName)
                ->setLastName($lastName)
                ->setBirthdate((new \DateTime())->sub(new \DateInterval('P'.rand(20, 50).'Y'.rand(0, 11).'M'.rand(0, 30).'D')))
                ->setFavoriteNumber(rand(0, 100))
            ;

            $em->persist($user);

            if ($i % 10000 == 0) {
                $em->flush();
            }
        }

        $em->flush();
    }
}