<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * Count records in collection.
     *
     * @return int
     */
    public function countAll()
    {
        return $this
            ->createQueryBuilder('entity')
            ->select('COUNT(entity)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
