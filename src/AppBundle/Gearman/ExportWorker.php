<?php

namespace AppBundle\Gearman;

use AppBundle\Entity\Export;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\Filesystem\Filesystem;

class ExportWorker extends AbstractWorker
{
    /**
     * Method name. Must correspond to correct method
     */
    const METHOD_EXPORT = 'export';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $exportDirectory;

    public function __construct(EntityManagerInterface $entityManager, $logDirectory, $exportDirectory)
    {
        parent::__construct($logDirectory);
        $this->entityManager = $entityManager;
        $this->exportDirectory = $exportDirectory;
    }

    /**
     * Mandatory method to tell the server what can we do
     */
    public function register()
    {
        $this->logMessage('Register '.self::METHOD_EXPORT.' function');
        $this->gmw->addFunction(self::METHOD_EXPORT, [$this, self::METHOD_EXPORT]);
    }

    /**
     * Export users into file
     */
    public function export(\GearmanJob $job)
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->logMessage('['.$job->handle().'] Starting job');

        $workload = $job->workload();
        $datas = unserialize($workload);

        $fs = new Filesystem();
        $fs->mkdir($this->exportDirectory);
        
        $userRepository = $this->entityManager->getRepository('AppBundle:User');

        $exportDate = new \DateTime();

        $filename = $datas['filename'];

        $offset = $datas['offset'];
        $limit = $datas['limit'];

        $exportString = '';
        $users = $userRepository->findBy([], null, $limit, $offset);
        $amountUsers = count($users);

        if ($amountUsers > 0) {
            $this->logMessage('['.$job->handle().'] Exporting '.$offset.' to '.($offset + $amountUsers));
            foreach ($users as $user) {
                $this->markReadOnly($user);
                $exportString .= '#'.$user->getId().','.$user->getUsername().','.$user->getFirstName().','.$user->getLastName().','.$user->getEmail().','.$user->getBirthdate()->format('d-m-Y').','.$user->getFavoriteNumber()."\n";

                $this->entityManager->persist((new Export())->setUser($user));
            }
            $this->entityManager->flush();

            file_put_contents($this->exportDirectory.'/'.$filename, $exportString, FILE_APPEND);
        } else {
            $this->logMessage('['.$job->handle().'] There is no users to export inside ['.$offset.', '.($offset + $limit).']');
        }

        $this->isJobDone = true;
    }

    /**
     * Mark entity as ready only if it's a managed entity only
     */
    public function markReadOnly($entity)
    {
        $uow = $this->entityManager->getUnitOfWork();
        if (UnitOfWork::STATE_MANAGED === $uow->getEntityState($entity)) {
            $uow->markReadOnly($entity);
        }
    }
}
