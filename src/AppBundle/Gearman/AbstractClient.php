<?php

namespace AppBundle\Gearman;

/**
 * This class can be herited for every client process, it has default methods to make it easier
 * The purpose is to tell the corresponding worker to do something
 * The link is done with the worker method name, defined as constant
 * The class must defined the run() method, wich should make a call to the gmc server
 * E.g. : addTask() + runTasks() (synchronous) http://php.net/manual/en/gearman.examples-reverse-task.php
 * E.g. : doBackground() (asynchronous) http://php.net/manual/en/gearman.examples-reverse-bg.php
 */
abstract class AbstractClient extends AbstractComponent
{
    /**
     * GearmanClient
     */
    protected $gmc;

    public function __construct($logDirectory)
    {
        parent::__construct($logDirectory);
    }

    public function init()
    {
        $this->gmc = new \GearmanClient();

        // Register the server
        $this->addServer($this->gmc);
        $this->logMessage('Starting client');
        
        return $this;
    }
    
    /**
     * Check if the task has been correctly did
     * return true if success, false otherwise
     */
    public function checkResult()
    {
        if ($this->gmc->returnCode() != GEARMAN_SUCCESS)
        {
            $this->logMessage('Bad return code received : '.$this->gmc->returnCode());
            return false;
        }

        $this->logMessage('Job successfully submitted');
        return true;
    }
}
