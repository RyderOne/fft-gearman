<?php

namespace AppBundle\Gearman;

/**
 * A worker is the opposite needed by client
 * It works in an infinite loop until an error occurs
 * It must be started with the symfony command gearman:worker:start [workerClassName] => see AppBundle\Command\StartWorkerCommand.php
 */
abstract class AbstractWorker extends AbstractComponent
{
    protected $isJobDone = false;

    /**
     * GearmanWorker
     */
    protected $gmw;

    public function __construct($logDirectory)
    {
        parent::__construct($logDirectory);

        $this->logMessage('Starting worker');

        $this->gmw = new \GearmanWorker();

        $this->addServer($this->gmw);

        $this->register();
    }

    public function work()
    {
        $this->logMessage('Waiting for jobs ...');
        while($this->gmw->work())
        {
            if ($this->gmw->returnCode() != GEARMAN_SUCCESS)
            {
                $this->logMessage('Bad return code received : '.$this->gwm->returnCode());
                break;
            }

            if ($this->isJobDone) {
                $this->logMessage('The job of this worker is done. For safety, we let it die silently.');
                break;
            }
        }
        $this->logMessage('This worker does not work anymore.');
    }

    /**
     * An implementation must defined register() method to tell the server wich method it can do
     */
    abstract protected function register();
}
