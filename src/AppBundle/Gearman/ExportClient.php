<?php

namespace AppBundle\Gearman;

use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Manage operations on club entity.
 *
 * @DI\Service("gearman.client.export")
 */
class ExportClient extends AbstractClient
{
    const LIMIT_PER_JOB = 1000;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @DI\InjectParams({
     *   "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *   "gearmanLogDirectory" = @DI\Inject("%gearman_log_directory%"),
     *   "kernelLogsDirectory" = @DI\Inject("%kernel.logs_dir%"),
     * })
     */
    public function __construct(EntityManagerInterface $entityManager, $gearmanLogDirectory, $kernelLogsDirectory)
    {
        $this->entityManager = $entityManager;

        parent::__construct($kernelLogsDirectory.'/'.$gearmanLogDirectory);
    }

    /**
     * Submit the task to the server
     * Since we use addTaskBackground(), the task is asynchronous
     * Use the checkResult to return if the task has been correctly submitted
     */
    public function run()
    {
        $this->logMessage('Submitting export jobs');

        $amountUsers = $this->entityManager->getRepository('AppBundle:User')->countAll();
        $offset = 0;
        $filename = date('H-i-s').'.csv';

        while ($amountUsers > $offset) {
            $limit = self::LIMIT_PER_JOB;

            $datas = [
                'offset' => $offset,
                'limit' => $limit,
                'filename' => $filename,
            ];

            $this->logMessage('Submitting task to export from '.$offset.' to '.($offset + $limit));
            $this->gmc->addTaskBackground(ExportWorker::METHOD_EXPORT, serialize($datas));

            $offset += $limit;
        }

        $this->gmc->runTasks();

        return $this->checkResult();
    }
}
