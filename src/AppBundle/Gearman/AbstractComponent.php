<?php

namespace AppBundle\Gearman;

use Symfony\Component\Filesystem\Filesystem;

/**
 * This class is used by Client and Worker
 * It contains helper methods and server registration
 */
abstract class AbstractComponent
{
    const DEFAULT_SERVER = [
        'host' => 'gearman',
        'port' => 4730
    ];

    /**
     * Default log directory
     * This value should be set at instance creation (worker or client)
     */
    public $logDirectory;

    public function __construct($logDirectory)
    {
        $this->logDirectory = $logDirectory.'/'.$this->getLoggableClassName();

        $fs = new Filesystem();
        $fs->mkdir($this->logDirectory);
    }

    protected function addServer($component)
    {
        $result = $component->addServer(self::DEFAULT_SERVER['host'], self::DEFAULT_SERVER['port']);
        if (!$result) {
            throw new \Exception('Unable to add server '.self::DEFAULT_SERVER['host'].':'.self::DEFAULT_SERVER['port']);
        }
    }

    /**
     * Log into file for gearman status.
     *
     * @param string $message
     */
    public function logMessage($message, $raw = false)
    {
        $filename = date('W-Y').'.log';
        if (!$raw) {
            $prefix = '['.date('Y-m-d H:i:s').']['.$this->getMemoryUsage().'] ';
            $suffix = PHP_EOL;
            $message = $prefix.$message.$suffix;
        }

        file_put_contents($this->getLogDirectory().'/'.$filename, $message, FILE_APPEND);
    }

    /**
     * Extract filename for log from class name
     */
    public function getLoggableClassName()
    {
        $parts = explode('\\', get_class($this));
        $name = end($parts);

        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $name, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('-', $ret);
    }

    /**
     * Gets the Default log directory.
     *
     * @return mixed
     */
    public function getLogDirectory()
    {
        return $this->logDirectory;
    }

    /**
     * Sets the Default log directory.
     *
     * @param mixed $logDirectory the log directory
     *
     * @return self
     */
    public function setLogDirectory($logDirectory)
    {
        $this->logDirectory = $logDirectory;

        return $this;
    }

    protected function getMemoryUsage()
    {
        $unit = ['B', 'KB', 'MB', 'GB'];
        $size = memory_get_usage(true);

        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2).' '.$unit[$i];
    }
}
