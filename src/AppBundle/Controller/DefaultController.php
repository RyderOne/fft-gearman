<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Export;
use AppBundle\Gearman\ExportClient;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Simple default index action.
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($request->get('export') == 1) {
            $this->get('gearman.client.export')
                ->init()
                ->run();

            $this->addFlash('success', 'Export done, you will receive a notification soon !');
        }

        return $this->render('default/index.html.twig');
    }
}
