var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    image = require('gulp-image'),
    newer = require('gulp-newer'),
    merge = require('merge-stream'),
    babel = require('gulp-babel');


function minifyIfNeeded() {
    return gutil.env.env === 'prod' || gutil.env.env === 'preprod' ? minify() : gutil.noop();
}

function uglifyIfNeeded() {
    return gutil.env.env === 'prod' || gutil.env.env === 'preprod' ? uglify() : gutil.noop();
}

var paths = {
    src: 'app/Resources/public/',
    bower: 'bower_components/',
    dest: 'web/',
    vendor: 'vendor/'
};

// Uploads fixtures
gulp.task('uploads', function() {
    return gulp.src(paths.src+'uploads/**/*')
        .pipe(gulp.dest(paths.dest+'uploads/'));
});

// Images
gulp.task('image', function () {
    var common = gulp.src(paths.src + 'common/img/*.{jpg,jpeg,png,gif}')
        .pipe(newer(paths.dest + 'img'))
        .pipe(image())
        .pipe(gulp.dest(paths.dest + 'img'));

    return merge(common);
});

// Styles
gulp.task('css', function () {

    var css = gulp.src([
        paths.bower + 'bootstrap/dist/css/bootstrap.min.css',
        paths.src + 'common/scss/*.scss',
        paths.src + 'front/scss/*.scss'
    ])
    .pipe(sass())
    .pipe(concat('main.css'))
    .pipe(minifyIfNeeded())
    .pipe(gulp.dest(paths.dest + 'css'));

    return merge(css);
});

// ES6
gulp.task('es6', function () {

    // Compile es6 with babel before concatenation
    var core = gulp.src([
        paths.src + 'common/js/es6/*.js',
        paths.src + 'front/js/es6/*.js'
    ])
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(concat('es6-processed.js'))
    .pipe(uglifyIfNeeded())
    .pipe(gulp.dest(paths.src + 'front/js'));

    return merge(core);
});

// JS
gulp.task('js', function () {

    var js = gulp.src([
        paths.bower + 'jquery/dist/jquery.min.js',
        paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
        paths.src + 'common/js/*.js',
        paths.src + 'front/js/*.js'
    ])
    .pipe(concat('main.js'))
    .pipe(uglifyIfNeeded())
    .pipe(gulp.dest(paths.dest + 'js'));

    return merge(js);
});

// Fonts
gulp.task('fonts', function() {
    var font = gulp.src([
        paths.bower + 'bootstrap/dist/fonts/*',
    ])
    .pipe(gulp.dest(paths.dest + 'fonts'));

    return merge(font);
});

// Default task
gulp.task('default', function() {
    gulp.start(['uploads', 'image', 'css', 'es6', 'js', 'fonts']);
});

// Watch task
gulp.task('watch', function() {
    gulp.watch([
        paths.src + 'common/js/es6/*.js',
        paths.src + 'front/js/es6/*.js'
    ], ['es6']);

    gulp.watch([
        paths.src + 'common/js/*.js',
        paths.src + 'front/js/*.js'
    ], ['js']);

    gulp.watch([
        paths.src + 'common/scss/*.scss',
        paths.src + 'front/scss/*.scss'
    ], ['css']);
});
